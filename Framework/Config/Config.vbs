'#######################################################################################
'# Script Name				: 
'# Script Description 		: This script contents global variables and file variables. 
'# Version Information		: Phase	1.0 
'# Client Name				: 
'# Date Created  			: 01/05/2020
'# Author					: 
'# Function					: 	 
'#######################################################################################
Option Explicit
Public fso					'	Parameter Create Object for Manage File
Public objExcel
Public objWorkBook
Public objWorkSheet
Public objWorkSheetDeposit
Public objWorkSheetBatch
Public objWorkSheetInstrument
Public objChildExcel
Public objChWorkBook
Public objChWorkSheet

Public myFile
Public testCase
Public dataRow
Public startExecuteDate
Public startExecuteTime
Public stopExecuteTime
'Public StartTimeExecuted
'Public StopTimeExecuted
'Public MyMstResultFile
'Public Action
'Public Status 
'Public strBrowser
'Public strPage
'Public strFrame
'public strMsgTxt
'public strFileName
'public strSheetName
'public strChildSheetName
'Public gstrOutputFile

'Navigation
Public objBrowser			'	Create Object Browser
Public objPage				'	Create Object Page
Public objWebTable			'	Create Object WebTable
Public strWebLink			'	String Define Object WebLink
Public strWebButton			'	String Define Object WebButton
Public strCellData			'	WebTable Get Cell Data


'Iteration Variables
Public ItrCnt
Public InstCnt

'Constant Declaration for Wait Property
Const WAIT_SHORT = 2
Const WAIT_MEDIUM = 5
Const WAIT_LONG = 10
Const WAIT_HALF = 30
Const WAIT_DOUBLE = 60

'Constant Declaration for General
Public strResultPathExecution			'Parameter for Create Folder SnapShot & Define Result name
Public strResultFileExecution			'Parameter for Create Folder SnapShot & Define Result name
Public strResultPathTestCase			'Parameter for Create Folder TestCase Result & SnapShot
Public strResultFileName				'Parameter for Create File Result Export from DataTable
Const DELIMITER = "|"
Const CAPTURE_SCREEN_SHOT_STATUS = "Capture"	'Flag Capture SnapShot Result Fail (True , Capture )

'Excel Config
'Path TestData
'Const Path_TestData="D:\Automate\WebTours\TestData\"
'Const Path_TestData="D:\murexframework\Data"
'C:\Program Files (x86)\Jenkins\workspace\UFT_AUTOMATE\Framework\
Const PATH_IMPORT_DATA = "C:\Jenkins\workspace\Trainning\TEST-GIT-UFT\Framework\Data"
'Const PATH_IMPORT_DATA = "D:\Extosoft\Bay\UFT\uft-framework\Framework\Data"
Const FILE_TEST_DATA = "TestData.xlsx"
'Const PATH_EXPORT_RESULT = "C:\Program Files (x86)\Jenkins\workspace\UFT_AUTOMATE\Framework\Result\"
Const PATH_EXPORT_RESULT = "C:\Jenkins\workspace\Trainning\TEST-GIT-UFT\Framework\Result\"
Const L_FILENAME_RESULT = "_Result"
Const R_FILENAME_RESULT = "Result_"
'Const StrTemp= strFolderpath & "\" & "Temp"
'Const ExcelType=".xlsx"

'Constant Declaration for Excel Result
Const RESULT_STATUS = "Status"					'string Column DataTable Result Status
Const RESULT_DESCRIPTION = "Description"		'string Column DataTable Result Description
Public strResultDescription
Public strResultStatus

'------------------------------ Scenario Control Started -----------------------------------------'
Public strScenario
Public strTag
'------------------------------ Scenario Control End -----------------------------------------'

'------------------------------ MCN (Get Content log from Mobile SMS) Application Parameter Started -----------------------------------------'
'	URL 
Const WebURL_MCN = ""

'------------------------------ User Modification End ---------------------------------------------'



